<?php
/**
 * @file
 * Commerce Discount weight module.
 */

/**
 * Implements hook_commerce_discount_rule_build().
 */
function commerce_discount_weight_commerce_discount_rule_build($rule, $discount) {
  // Change rule's weight.
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount);
  if ($discount_wrapper->__isset('commerce_discount_weight')) {
    $weight = $discount_wrapper->commerce_discount_weight->value();
    if (!is_null($weight)) {
      $rule->weight = $weight;
    }
  }
}

/**
 * Implements hook_views_api().
 */
function commerce_discount_weight_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'commerce_discount_weight') . '/includes/views',
  );
}

/**
 * Submit handler for Views Weight form.
 */
function commerce_discount_weight_views_submit($form, &$form_state) {
  $values = $form_state['values'];

  foreach ($values['discount_ids'] as $key => $discount_id) {
    $discount = entity_load('commerce_discount', array($discount_id));
    $discount = reset($discount);
    $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount);
    $discount_wrapper->commerce_discount_weight = $values['weight_form'][$key][$discount_id];
    $discount_wrapper->save();
  }

  drupal_set_message(t('Weights saved successfully.'));
}

/**
 * Implementes hook_preprocess_views_view_table().
 */
function commerce_discount_weight_preprocess_views_view_table(&$variables) {
  // If there isn't a Weight field or the user doesn't have access to it, don't do anything.
  if (!array_key_exists('weight_form', $variables['fields'])) {
    return;
  }

  // Add the tabledrag attributes.
  $vars['classes_array'][] = 'draggable';
  foreach ($variables['rows'] as $key => $row) {
    $variables['row_classes'][$key][] = 'draggable';
  }
  $table_id = 'commerce-discount-weight-form-table-' . $variables['id'];
  $variables['attributes_array']['id'] = $table_id;

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'commerce-discount-weight-form');
}
