<?php
/**
 * @file
 * Views data for Commerce Discount Weight.
 */

/**
 * Implements hook_views_data().
 */
function commerce_discount_weight_views_data() {

  $data['commerce_discount']['weight_form'] = array(
    'title' => t('Commerce discount Weight form'),
    'help' => t('The weight of the commerce discount, formatted as a form element.'),
    'real field' => 'discount_id',
    'field' => array(
      'handler' => 'commerce_discount_weight_handler_weight_form',
      'click sortable' => TRUE,
      'access callback' => 'user_access',
      'access arguments' => array('administer commerce discounts'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  return $data;
}
