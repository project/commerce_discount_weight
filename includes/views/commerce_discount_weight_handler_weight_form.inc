<?php
/**
 * @file
 * Views handler for commerce discount weight, inspired by weight module.
 */

/**
 * Class commerce_discount_weight_handler_weight_form
 */
class commerce_discount_weight_handler_weight_form extends views_handler_field {

  function init(&$view, &$options) {
    parent::init($view, $options);
    $this->additional_fields['discount_id'] = array('table' => 'commerce_discount', 'field' => 'discount_id');
    $this->additional_fields['commerce_discount_weight'] = array('table' => 'field_data_commerce_discount_weight', 'field' => 'commerce_discount_weight_value');
  }

  function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    // Initialize $discount_ids array to store discount_ids for saving weights.
    $discount_ids = array();

    $field_name = $this->options['id'];
    $form[$field_name] = array(
      '#tree' => TRUE,
    );

    // At this point, the query has already been run, so we can access the results
    foreach ($this->view->result as $row_id => $row) {
      if (isset($row->field_data_commerce_discount_weight_commerce_discount_weight)
        && (
          !empty($row->field_data_commerce_discount_weight_commerce_discount_weight)
          || $row->field_data_commerce_discount_weight_commerce_discount_weight === "0"
        )) {
        $weight = $row->field_data_commerce_discount_weight_commerce_discount_weight;
      }
      else {
        $weight = 0;
      }
      $discount_ids[] = $row->discount_id;

      $form[$field_name][$row_id][$row->discount_id] = array(
        '#type' => 'textfield',
        '#default_value' => $weight,
        '#attributes' => array('class' => array('commerce-discount-weight-form')),
        '#access' => user_access('administer commerce discounts'),
      );
    }

    $form['discount_ids'] = array(
      '#type' => 'value',
      '#value' => $discount_ids,
    );

    $form['#submit'][] = 'commerce_discount_weight_views_submit';
    $form['#action'] = request_uri();
  }
}
