<?php
/**
 * @file
 * Dfault views for Commerce Discount weight.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_discount_weight_views_default_views() {
  $view = new view();
  $view->name = 'commerce_discount_weight';
  $view->description = 'Display a list of orderable discounts for store admin.';
  $view->tag = 'default';
  $view->base_table = 'commerce_discount';
  $view->human_name = 'Discounts order';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Order discounts';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer commerce discounts';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'label' => 'label',
    'type_1' => 'type_1',
    'type' => 'type',
    'commerce_discount_usage' => 'commerce_discount_usage',
    'status' => 'status',
    'operations_dropbutton' => 'operations_dropbutton',
    'weight_form' => 'weight_form',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'label' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_discount_usage' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 0,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'operations_dropbutton' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'weight_form' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* Comportement en l'absence de résultats: Global : Zone de texte */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['content'] = 'No discounts found.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relation: Entity Reference : Entité référencée */
  $handler->display->display_options['relationships']['commerce_discount_offer_target_id']['id'] = 'commerce_discount_offer_target_id';
  $handler->display->display_options['relationships']['commerce_discount_offer_target_id']['table'] = 'field_data_commerce_discount_offer';
  $handler->display->display_options['relationships']['commerce_discount_offer_target_id']['field'] = 'commerce_discount_offer_target_id';
  $handler->display->display_options['relationships']['commerce_discount_offer_target_id']['required'] = TRUE;
  /* Champ: Commerce Discount : Étiquette */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'commerce_discount';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['label'] = 'Name';
  /* Champ: Commerce Discount Offer : Type */
  $handler->display->display_options['fields']['type_1']['id'] = 'type_1';
  $handler->display->display_options['fields']['type_1']['table'] = 'commerce_discount_offer';
  $handler->display->display_options['fields']['type_1']['field'] = 'type';
  $handler->display->display_options['fields']['type_1']['relationship'] = 'commerce_discount_offer_target_id';
  $handler->display->display_options['fields']['type_1']['label'] = 'Offer';
  /* Champ: Commerce Discount : Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'commerce_discount';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['type']['alter']['text'] = '<span>[type]</span>';
  /* Champ: Commerce Discount : Analytics */
  $handler->display->display_options['fields']['commerce_discount_usage']['id'] = 'commerce_discount_usage';
  $handler->display->display_options['fields']['commerce_discount_usage']['table'] = 'commerce_discount';
  $handler->display->display_options['fields']['commerce_discount_usage']['field'] = 'commerce_discount_usage';
  /* Champ: Commerce Discount : Statut */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'commerce_discount';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['type'] = 'active-disabled';
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Champ: Commerce Discount : Actions */
  $handler->display->display_options['fields']['operations_dropbutton']['id'] = 'operations_dropbutton';
  $handler->display->display_options['fields']['operations_dropbutton']['table'] = 'commerce_discount';
  $handler->display->display_options['fields']['operations_dropbutton']['field'] = 'operations_dropbutton';
  /* Champ: Commerce Discount : Commerce discount Weight form */
  $handler->display->display_options['fields']['weight_form']['id'] = 'weight_form';
  $handler->display->display_options['fields']['weight_form']['table'] = 'commerce_discount';
  $handler->display->display_options['fields']['weight_form']['field'] = 'weight_form';
  /* Critère de tri: Commerce Discount : Set discount weight. (commerce_discount_weight) */
  $handler->display->display_options['sorts']['commerce_discount_weight_value']['id'] = 'commerce_discount_weight_value';
  $handler->display->display_options['sorts']['commerce_discount_weight_value']['table'] = 'field_data_commerce_discount_weight';
  $handler->display->display_options['sorts']['commerce_discount_weight_value']['field'] = 'commerce_discount_weight_value';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'admin/commerce/store/discounts/weight';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Order discounts';
  $handler->display->display_options['menu']['description'] = 'Define weight and so order discounts.';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['commerce_discount_weight'] = array(
    t('Display a list of discounts for store admin'),
    t('Order discounts'),
    t('Define weight and so order discounts.'),
    t('Commerce discount Weight form'),
  );

  $views[$view->name] = $view;

  return $views;
}
