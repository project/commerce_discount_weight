<?php
/**
 * @file
 * Install commerce discount weight module.
 */

/**
 * Implements hook_install().
 *
 * Create a date field for commerce discounts.
 * The instance will be added for every newley created bundle.
 *
 * @see commerce_discount_modules_enabled()
 */
function commerce_discount_weight_install() {
  _commerce_discount_weight_install_helper();
}

/**
 * Create commerce_discount_weight field and instances.
 *
 * @throws \Exception
 * @throws \FieldException
 */
function _commerce_discount_weight_install_helper(){
  // Clear field info cache, so entity reference field can be used.
  field_info_cache_clear();

  $fields = field_info_fields();
  $instances = field_info_instances();

  if (empty($fields['commerce_discount_weight'])) {
    $field = array(
      'entity_types' => array('commerce_discount'),
      'settings' => array(
        'max_length' => 10,
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'field_name' => 'commerce_discount_weight',
      'type' => 'text',
      'locked' => TRUE,
    );
    field_create_field($field);
  }
  foreach (commerce_discount_types() as $type => $info) {
    if (empty($instances['commerce_discount'][$type]['commerce_discount_weight'])) {
      $instance = array(
        'field_name' => 'commerce_discount_weight',
        'entity_type' => 'commerce_discount',
        'bundle' => $type,
        'label' => t('Commerce discount weight'),
        'description' => t('Weight let you order your discounts.'),
        'required' => TRUE,
        'widget' => array(
          'weight' => 100,
        ),
        'default_value' => array(
          0 => array(
            'value' => '0',
          ),
        ),
      );
      field_create_instance($instance);
    }
  }
}

/**
 * Implements hook_uninstall().
 */
function commerce_discount_weight_uninstall() {
  field_delete_field('commerce_discount_weight');
}
